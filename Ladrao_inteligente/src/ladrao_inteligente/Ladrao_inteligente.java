

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import ladrao_inteligente.ClassValue;

/**
 *
 * @author  Kevin Veloso 11318626 e Richelieu Costa 11328634
 */
public class Ladrao_inteligente {

	static ArrayList<Integer> pesos = new ArrayList<Integer>();
	static ArrayList<Integer> lucros = new ArrayList<Integer>();
     
       
    static int capacidade_mochila;
    static int qtd_itens;
       
    /**
     * @param args the command line arguments
     * @throws IOException 
     */
    

	public static void instancia1(){
		
		  qtd_itens=7;
	        capacidade_mochila=23;
	        
	        
	        pesos.add(1);
	        pesos.add(2);
	        pesos.add(5);
	        pesos.add(6);
	        pesos.add(7);
	        pesos.add(9);
	        pesos.add(11);
	        
	        lucros.add(1);
	        lucros.add(6);
	        lucros.add(18);
	        lucros.add(22);
	        lucros.add(28);
	        lucros.add(40);
	        lucros.add(60);
	        
		
	}
	
	
	public static void instancia2(){
		
		  qtd_itens=5;
	        capacidade_mochila=10;
	        
	        
	        pesos.add(8);
	        pesos.add(3);
	        pesos.add(6);
	        pesos.add(4);
	        pesos.add(2);
	        
	        lucros.add(100);
	        lucros.add(60);
	        lucros.add(70);
	        lucros.add(15);
	        lucros.add(15);
	        
		
	}
	
	public static void instanciaSala(){
		
		  qtd_itens=4;
	        capacidade_mochila=6;
	        
	        
	        pesos.add(3);
	        pesos.add(4);
	        pesos.add(4);
	        pesos.add(3);
	        
	        
	        lucros.add(3);
	        lucros.add(4);
	        lucros.add(5);
	        lucros.add(3);
	        
		
	}
	
	
	
    public static void main(String[] args) throws IOException {
        
    
        
     
     instanciaSala();
   //   instancia1();
     // instancia2();
 
        System.out.println(pesos);
        System.out.println(lucros);
        
        HashMap<String, ClassValue> tabela = new HashMap<String, ClassValue>();
        
        int index_produto = pesos.size() - 1;
        int capacidade = 0;
        
        while(capacidade <= capacidade_mochila) {
            
            if((capacidade - pesos.get(index_produto)) < 0) {
                
                ClassValue valor = new ClassValue(0, false);
                tabela.put(capacidade+","+(index_produto), valor);
                
            }else {
                
                ClassValue valor = new ClassValue(lucros.get(index_produto), true);
                tabela.put((capacidade+","+(index_produto )), valor);
                
            }
            System.out.println(" Coluna "+(index_produto)+": "+(capacidade+","+(index_produto )) +" -> "+ tabela.get((capacidade+","+(index_produto ))).getLucro());
            
            capacidade++; 
        }
                
        index_produto--;
        
        
        while(index_produto >= 0) {
                   
            capacidade = 0;
        
            while(capacidade <= capacidade_mochila) {
                
                if((capacidade - pesos.get(index_produto)) < 0) {
                    
                    ClassValue valor = new ClassValue(tabela.get(capacidade+","+(index_produto+1)).getLucro(), false);
                    tabela.put((capacidade+","+(index_produto)), valor);
                    
                } else {
                    
                    ClassValue max = max(tabela.get(capacidade+","+(index_produto+1)).getLucro(), tabela.get((capacidade-pesos.get(index_produto))+","+ (index_produto+1)).getLucro() + lucros.get(index_produto));
                    tabela.put((capacidade+","+(index_produto)), max);  
                    
                }
                
                System.out.println(" Coluna "+(index_produto )+": "+(capacidade+","+(index_produto )) +" -> "+ tabela.get((capacidade+","+(index_produto))).getLucro());
                  
                capacidade++;
            }
            
           index_produto--;
        }
        
                
        capacidade = capacidade_mochila;
        
        boolean[] produtos_roubados = new boolean[pesos.size()];
                
        ClassValue instancia = null;
    
        for(int index_prod = 0; index_prod < pesos.size(); index_prod++) {
                        
            instancia = tabela.get((capacidade+","+(index_prod)));

                        
            if(instancia.getPego()) {
                
                produtos_roubados[index_prod] = true;
                
                capacidade = capacidade - pesos.get(index_prod);
                
                
            } else {
                
                produtos_roubados[index_prod] = false;
                
            }
                        
        }
        
        for(int i=0; i < produtos_roubados.length; i++) {
            System.out.println("Produto "+(i)+": "+produtos_roubados[i]);
        }
        
        
    }
    
    public static ClassValue max(int valor1, int valor2){
        int lucro;
        boolean x;

        if( valor1 > valor2) {

            x = false;
            lucro = valor1;


        } else {

            x = true;
            lucro = valor2;


        }


        return new ClassValue(lucro, x);
    }
}
   