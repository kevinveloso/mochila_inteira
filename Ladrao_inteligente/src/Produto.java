/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kevin
 */
public class Produto {
    
    boolean pego;
    int peso, lucro;
    
    public Produto(int peso, int lucro) {
        this.peso = peso;
        this.lucro = lucro;
        this.pego = false;
    }
    
    
    public int getPeso() {
        return this.peso;
    }
    
    public int getLucro() {
        return this.lucro;
    }
    
    public void setPego(boolean pego) {
        this.pego = pego;
    }
    
}
